var dsnv = [];
const DSNV = "DSNV";
let dsnvLocalStorage = localStorage.getItem(DSNV);

if (JSON.parse(dsnvLocalStorage)) {
    var data = JSON.parse(dsnvLocalStorage);
    console.log("before ", data);
    for (var index = 0; index < data.length; index++) {
      var current = data[index];
      var nv = new NhanVien(
        current.user,
        current.hoTen,
        current.email,
        current.matKhau,
        current.ngayLam,
        current.luongCoBan,
        current.chucVu,
        current.gioLam,

      );
      dsnv.push(nv);
    }
    renderDanhSachNhanVien(dsnv);
  }

  function saveLocalStorage() {
    //  convert array thành json
  
    var dsnvJson = JSON.stringify(dsnv);
   
    localStorage.setItem(DSNV, dsnvJson);
  }
  function themNV(){
    var newNV = layThongTinTuForm();

  dsnv.push(newNV);
  saveLocalStorage();
  renderDanhSachNhanVien(dsnv);

  }

  function xoaNV(id) {
    var index = timKiemViTri(id, dsnv);
    console.log("index: ", index);
    if (index !== -1) {
      // console.log("before", dssv.length);
  
      dsnv.splice(index, 1);
      saveLocalStorage();
      // console.log("after", dssv.lengt);
      renderDanhSachSinhVien(dsnv);
    }
  }

  function suaNV(id) {
    var index = timKiemViTri(id, dsnv);
    if (index !== -1) {
      var nv = dsnv[index];
      showThongTinLenForm(nv);
    }
  }

  function capNhatNV() {
    var nvEdited = layThongTinTuForm();
    let index = timKiemViTri(nvEdited.user, dsnv);
  
    if (index !== -1) {
      dsnv[index] = nvEdited;
      saveLocalStorage();
  
      renderDanhSachSinhVien(dsnv);
    }
  }
  
  function reSetForm(){
    document.getElementById("formQLSV").reset();
  }