function NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLam){
    this.user = taiKhoan;
    this.ten = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayLam = ngayLam;
    this.luongCoBan = luongCoBan;
    this.chucVu = chucVu;
    this.giolam = gioLam;
    
    this.tongLuong = function(){
        if(this.chucVu == "Sếp"){
            return this.luongCoBan*3;
        } else if (this.chucVu == "Trưởng phòng"){
            return this.luongCoBan*2;
        } else {
            return this.luongCoBan*1;
        }
    };
    this.xepLoai = function(){
        if(this.giolam*1 >= 192){
            return "Xuất sắt";
        } else if(this.giolam*1 >= 176){
            return "Giỏi";
        } else if(this.giolam*1 >= 160){
            return "Khá";
        } else if(this.giolam*1 < 160){
            return "Trung Bình";
        }
    };
}